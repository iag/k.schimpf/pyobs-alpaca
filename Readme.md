# Pyobs Alpaca Server

This project provides a http interface according to the 
ASCOM Alpaca Device API (https://www.ascom-standards.org/api/#/) for
pyobs.

## Example Configuration

```yaml
class: pyobs_alpaca_server.device.Device
devices:
  telescope:
    - telescope
  camera:
    - camera

comm:
  class: pyobs.comm.xmpp.XmppComm
  jid: alpaca@pyobs.test.de
  server: localhost:5222
  password: pyobs
````
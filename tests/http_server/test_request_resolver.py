import pytest
from aiohttp.test_utils import make_mocked_request

from pyobs_alpaca_server.http_server.request_resolver import request_resolver, to_type, compile_route_parameters, \
    compile_body_parameters


@pytest.fixture()
def test_function():
    async def test_function(device_type, device_number, connected):
        return device_type, device_number, connected

    return test_function


@pytest.fixture()
def resolver(test_function):
    return request_resolver(test_function, ["device_type", "device_number"], ["Connected"])


def test_to_type():
    assert to_type("str", str) == "str"

    assert to_type("true", bool) == True
    assert to_type("True", bool) == True

    assert to_type("1.2", float) == 1.2
    assert to_type("1", int) == 1


def test_compile_route_parameters():
    req = make_mocked_request('GET', '/')
    req['info'] = {
        "device_type": "camera",
        "device_number": "0"
    }

    output = compile_route_parameters(req, [("device_type", str), ("device_number", int)])
    assert output["device_type"] == "camera"
    assert output["device_number"] == 0


@pytest.mark.asyncio
async def test_compile_body_parameters(mocker):
    req = make_mocked_request('POST', '/')
    mocker.patch("aiohttp.web.Request.post", return_value={"Connected": "true"})

    output = await compile_body_parameters(req, [("Connected", bool)])
    assert output["connected"]


@pytest.mark.asyncio
async def test_request_resolver(mocker, test_function):
    req = make_mocked_request('POST', '/')
    req['info'] = {
        "device_type": "camera",
        "device_number": "0"
    }
    mocker.patch("aiohttp.web.Request.post", return_value={"Connected": "true"})

    resolved_func = request_resolver(test_function, [("device_type", str), ("device_number", int)], [("Connected", bool)])

    assert await resolved_func(req) == ("camera", 0, True)

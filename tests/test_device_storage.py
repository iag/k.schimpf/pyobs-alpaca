import pytest
from pyobs.comm import Comm

from pyobs_alpaca_server.device_storage import DeviceStorage, BadDeviceType, BadDeviceNumber


def test_device_storage_init():
    devices = {
        "telescope": ["telescope", "another_telescope"],
        "camera": ["camera", "another_camera"]
    }
    storage = DeviceStorage(devices, Comm())

    assert storage._devices["telescope"][0].get_name() == "telescope"
    assert storage._devices["telescope"][1].get_name() == "another_telescope"

    assert storage._devices["camera"][0].get_name() == "camera"
    assert storage._devices["camera"][1].get_name() == "another_camera"


def test_device_storage_get_device():
    devices = {
        "telescope": ["telescope", "another_telescope"],
        "camera": ["camera", "another_camera"]
    }
    storage = DeviceStorage(devices, Comm())

    assert storage.get_device("telescope", 0).get_name() == "telescope"
    assert storage.get_device("telescope", 1).get_name() == "another_telescope"
    assert storage.get_device("camera", 0).get_name() == "camera"
    assert storage.get_device("camera", 1).get_name() == "another_camera"


def test_device_storage_get_device_invalid():
    devices = {
        "telescope": ["telescope", "another_telescope"],
        "camera": ["camera", "another_camera"]
    }
    storage = DeviceStorage(devices, Comm())

    with pytest.raises(BadDeviceType):
        storage.get_device("does not exist", 0)

    with pytest.raises(BadDeviceNumber):
        storage.get_device("camera", 3)

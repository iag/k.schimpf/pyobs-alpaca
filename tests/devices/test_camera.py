from typing import Any, List

import pyobs
import pytest
from pyobs.comm import Comm
from pyobs.interfaces import ICamera, IImageFormat
from pyobs.modules.camera import DummyCamera
from pyobs.utils.enums import ImageFormat, ImageType, ExposureStatus

import tests.devices.test_camera
from pyobs_alpaca_server.devices.camera import CameraState


class DummyFrameCamera(IImageFormat):
    def __init__(self, image_format: ImageFormat):
        self._image_format = image_format

    async def get_image_format(self, **kwargs: Any) -> ImageFormat:
        return self._image_format

    async def list_image_formats(self, **kwargs: Any) -> List[str]:
        pass

    async def set_image_format(self, fmt: ImageFormat, **kwargs: Any) -> None:
        self._image_format = fmt


@pytest.fixture(scope="module")
def camera_state():
    return CameraState("camera", Comm())


def test_camera_state_init():
    comm = Comm()
    camera_state = CameraState("camera", comm)

    assert camera_state._name == "camera"
    assert camera_state._camera is None
    assert camera_state._comm == comm
    assert camera_state._last_exposure is None
    assert camera_state._pix_size is None


def test_camera_state_get_name(camera_state):
    assert camera_state.get_name() == "camera"


@pytest.mark.asyncio
async def test_telescope_connect(mocker):
    comm = Comm()
    camera_state = CameraState("camera", comm)

    mocker.patch('pyobs.comm.Comm.proxy')
    await camera_state.connect()
    pyobs.comm.Comm.proxy.assert_called_once_with("camera", ICamera)


def test_camera_disconnect():
    camera_state = CameraState("camera", Comm())
    camera_state._camera = DummyCamera()
    camera_state.disconnect()
    assert camera_state._camera is None


def test_camera_is_connected():
    camera_state = CameraState("camera", Comm())
    assert not camera_state.is_connected()

    camera_state._camera = DummyCamera()
    assert camera_state.is_connected()


@pytest.mark.asyncio
async def test_camera_set_format(mocker):
    camera_state = CameraState("camera", Comm())
    camera_state._camera = DummyFrameCamera(ImageFormat.INT8)
    mocker.patch('tests.devices.test_camera.DummyFrameCamera.set_image_format')

    await camera_state.set_format(ImageFormat.INT8)
    tests.devices.test_camera.DummyFrameCamera.set_image_format.assert_called_with(ImageFormat.INT8)

    await camera_state.set_format(ImageFormat.INT16)
    tests.devices.test_camera.DummyFrameCamera.set_image_format.assert_called_with(ImageFormat.INT16)


@pytest.mark.asyncio
async def test_camera_get_format():
    camera_state = CameraState("camera", Comm())
    camera_state._camera = DummyCamera()

    assert await camera_state.get_format() == ImageFormat.INT8

    camera_state._camera = DummyFrameCamera(ImageFormat.INT16)
    assert await camera_state.get_format() == ImageFormat.INT16


@pytest.mark.asyncio
async def test_camera_get_bit_depth():
    camera_state = CameraState("camera", Comm())

    camera_state._camera = DummyFrameCamera(ImageFormat.INT8)
    assert await camera_state.get_bit_depth() == 8

    await camera_state.set_format(ImageFormat.INT16)
    assert await camera_state.get_bit_depth() == 16

    await camera_state.set_format(ImageFormat.FLOAT32)
    assert await camera_state.get_bit_depth() == 32

    await camera_state.set_format(ImageFormat.FLOAT64)
    assert await camera_state.get_bit_depth() == 64

    await camera_state.set_format(ImageFormat.RGB24)
    assert await camera_state.get_bit_depth() == 24


@pytest.mark.asyncio
async def test_camera_get_window(mocker):
    camera_state = CameraState("camera", Comm())
    camera_state._camera = DummyCamera()
    mocker.patch('pyobs.modules.camera.DummyCamera.get_window', return_value=[2, 2, 2, 2])

    assert await camera_state.get_window() == [2, 2, 2, 2]


@pytest.mark.asyncio
async def test_camera_get_temperature(mocker):
    camera_state = CameraState("camera", Comm())
    camera_state._camera = DummyCamera()
    mocker.patch('pyobs.modules.camera.DummyCamera.get_temperatures', return_value={'CCD': 10})
    assert await camera_state.get_temperature() == 10


@pytest.mark.asyncio
async def test_camera_get_binning(mocker):
    camera_state = CameraState("camera", Comm())
    camera_state._camera = DummyCamera()
    mocker.patch('pyobs.modules.camera.DummyCamera.get_binning', return_value=(2, 2))
    assert await camera_state.get_binning() == (2, 2)


@pytest.mark.asyncio
async def test_camera_set_binning(mocker):
    camera_state = CameraState("camera", Comm())
    camera_state._camera = DummyCamera()
    mocker.patch('pyobs.modules.camera.DummyCamera.set_binning')
    mocker.patch('pyobs.modules.camera.DummyCamera.list_binnings', return_value=[[2, 2], [3, 3]])

    await camera_state.set_binning(2)
    pyobs.modules.camera.DummyCamera.set_binning.assert_called_with(2, 2)

    await camera_state.set_binning(3)
    pyobs.modules.camera.DummyCamera.set_binning.assert_called_with(3, 3)

    with pytest.raises(ValueError):
        await camera_state.set_binning(4)


@pytest.mark.asyncio
async def test_camera_expose(mocker):
    camera_state = CameraState("camera", Comm())
    camera_state._camera = DummyCamera()

    exposure_time_spy = mocker.spy(camera_state._camera, "set_exposure_time")
    image_type_spy = mocker.spy(camera_state._camera, "set_image_type")
    grab_spy = mocker.spy(camera_state._camera, "grab_data")

    await camera_state.expose(10, True)
    exposure_time_spy.assert_called_with(10)
    image_type_spy.assert_called_with(ImageType.OBJECT)
    grab_spy.assert_called_once()
    assert await camera_state._camera.get_exposure_status() == ExposureStatus.IDLE
    assert camera_state._last_exposure is not None

    await camera_state.expose(10, False)
    image_type_spy.assert_called_with(ImageType.DARK)


@pytest.mark.asyncio
async def test_camera_img_ready(mocker):
    camera_state = CameraState("camera", Comm())
    camera_state._camera = DummyCamera()

    await camera_state.expose(10, True)
    assert await camera_state.img_ready()


@pytest.mark.asyncio
async def test_camera_img_ready(mocker):
    camera_state = CameraState("camera", Comm())
    camera_state._camera = DummyCamera()
    mocker.patch("pyobs.modules.camera.DummyCamera.grab_data", return_value="test_image")

    await camera_state.expose(0, True)

    assert await camera_state.get_img() == "test_image"

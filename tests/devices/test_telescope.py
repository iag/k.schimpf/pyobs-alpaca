import pyobs
import pytest as pytest
from pyobs.comm import Comm
from pyobs.interfaces import ITelescope
from pyobs.modules.telescope import DummyTelescope

from pyobs_alpaca_server.devices.telescope import TelescopeState

@pytest.fixture(scope="module")
def telescope_state():
    return TelescopeState("telescope", Comm())


def test_telescope_state_init():
    comm = Comm()
    telescope_state = TelescopeState("telescope", comm)

    assert telescope_state._name == "telescope"
    assert telescope_state._telescope is None
    assert telescope_state._comm == comm
    assert not telescope_state._is_pulse_guiding
    assert telescope_state._guide_rate_dec == 0.01
    assert telescope_state._guide_rate_ra == 0.01


def test_telescope_get_name(telescope_state):
    assert telescope_state.get_name() == "telescope"


@pytest.mark.asyncio
async def test_telescope_connect(mocker):
    comm = Comm()
    telescope_state = TelescopeState("telescope", comm)

    mocker.patch('pyobs.comm.Comm.proxy')
    await telescope_state.connect()
    pyobs.comm.Comm.proxy.assert_called_once_with("telescope", ITelescope)


def test_telescope_disconnect(telescope_state):
    telescope_state._telescope = DummyTelescope()
    telescope_state.disconnect()
    assert telescope_state._telescope is None


def test_telescope_is_connected(telescope_state):
    assert not telescope_state.is_connected()

    telescope_state._telescope = DummyTelescope()
    assert telescope_state.is_connected()


def test_telescope_get_guide_rates(telescope_state):
    telescope_state._guide_rate_ra = 10
    telescope_state._guide_rate_dec = 10
    assert telescope_state.get_guide_rates() == (10, 10)


def test_telescope_set_guide_rate_dec(telescope_state):
    guide_rate = 10
    telescope_state.set_guide_rate_dec(guide_rate)
    assert telescope_state._guide_rate_dec == guide_rate


def test_telescope_set_guide_rate_ra(telescope_state):
    guide_rate = 10
    telescope_state.set_guide_rate_ra(guide_rate)
    assert telescope_state._guide_rate_ra == guide_rate


def test_telescope_is_pulse_guiding(telescope_state):
    assert not telescope_state._is_pulse_guiding
    telescope_state._is_pulse_guiding = True
    assert telescope_state._is_pulse_guiding


def test_telescope_calc_pulse():
    telescope_state = TelescopeState("telescope", Comm())
    assert telescope_state._calc_pulse(0, 1000) == (0, 0.01)
    assert telescope_state._calc_pulse(1, 1000) == (0, -0.01)
    assert telescope_state._calc_pulse(2, 1000) == (0.01, 0)
    assert telescope_state._calc_pulse(3, 1000) == (-0.01, 0)


def test_telescope_calc_pulse_invalid():
    telescope_state = TelescopeState("telescope", Comm())

    with pytest.raises(ValueError):
        telescope_state._calc_pulse(4, 1000)


@pytest.mark.asyncio
async def test_telescope_pulse_guiding(mocker):
    telescope_state = TelescopeState("telescope", Comm())

    telescope_state._telescope = DummyTelescope()
    mocker.patch('pyobs.modules.telescope.DummyTelescope.set_offsets_radec')

    await telescope_state.pulse_guide(0, 1000)
    pyobs.modules.telescope.DummyTelescope.set_offsets_radec.assert_called_with(0, 0.01)

    await telescope_state.pulse_guide(1, 1000)
    pyobs.modules.telescope.DummyTelescope.set_offsets_radec.assert_called_with(0, -0.01)

    await telescope_state.pulse_guide(2, 1000)
    pyobs.modules.telescope.DummyTelescope.set_offsets_radec.assert_called_with(0.01, 0)

    await telescope_state.pulse_guide(3, 1000)
    pyobs.modules.telescope.DummyTelescope.set_offsets_radec.assert_called_with(-0.01, 0)


@pytest.mark.asyncio
async def test_telescope_pulse_invalid():
    telescope_state = TelescopeState("telescope", Comm())

    with pytest.raises(ConnectionError):
        await telescope_state.pulse_guide(0, 1000)

    telescope_state._is_pulse_guiding = True
    with pytest.raises(PermissionError):
        await telescope_state.pulse_guide(0, 1000)

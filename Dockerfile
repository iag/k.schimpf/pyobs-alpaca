FROM python:3.9.0

WORKDIR usr/app/src

COPY setup.py .
ADD pyobs_alpaca_server ./pyobs_alpaca_server

RUN pip install setuptools --no-cache-dir
RUN pip install . --no-cache-dir

COPY pyobs-config.yaml .

EXPOSE 8080
CMD ["pyobs", "pyobs-config.yaml"]
from setuptools import setup

setup(
   name='pyobs_alpaca_server',
   version='1.0',
   description='ASCOM Alpaca Server for pyobs',
   author='Karsten Schimpf',
   author_email='k.schimpf@stud.uni-goettingen.de',
   packages=['pyobs_alpaca_server', 'pyobs_alpaca_server.http_server', 'pyobs_alpaca_server.devices'],
   install_requires=['pyobs-core', 'aiohttp']
)
import asyncio

import logging
from typing import Any


from aiohttp import web
from pyobs.images import Image
from pyobs.modules import Module
from pyobs.utils.enums import MotionStatus

from pyobs_alpaca_server.device_storage import DeviceStorage
from pyobs_alpaca_server.devices.camera import CameraState
from pyobs_alpaca_server.http_server.request_resolver import request_resolver
from pyobs_alpaca_server.response import BoolResponse, BasicResponse, ImageArrayResponse, DoubleResponse, \
    IntegerResponse
from pyobs_alpaca_server.http_server.server import Server
from pyobs_alpaca_server.devices.telescope import TelescopeState


class AlpacaAPI(Module):
    def __init__(self, devices, host: str = 'localhost', ip: int = 8080, **kwargs: Any):
        super().__init__(**kwargs)

        self._devices = DeviceStorage(devices, self.comm)

        self.prefix = '/api/v1/{device_type}/{device_number}/'
        self.server = Server(host, ip)

        self.server.router.add_routes(
            [
                web.route("GET", "/ping", self.ping),
                web.route("POST", self.prefix + "action", self.action),
                web.route("GET", self.prefix + "connected", request_resolver(self.get_connected, [("device_type", str), ("device_number", int)])),
                web.route("PUT", self.prefix + "connected", request_resolver(self.set_connected, [("device_type", str), ("device_number", int)], [("Connected", bool)])),
            ])

        self.cam_prefix = '/api/v1/camera/{device_number}/'
        self.server.router.add_routes(
            [
                web.route("GET", self.cam_prefix + "binx", request_resolver(self.get_binx, [("device_number", int)])),
                web.route("GET", self.cam_prefix + "biny", request_resolver(self.get_biny, [("device_number", int)])),
                web.route("PUT", self.cam_prefix + "binx", request_resolver(self.set_binx, [("device_number", int)], [("BinX", int)])),
                web.route("PUT", self.cam_prefix + "biny", request_resolver(self.set_biny, [("device_number", int)], [("BinY", int)])),
                web.route("GET", self.cam_prefix + "camaraxsize", request_resolver(self.get_camaraxsize, [("device_number", int)])),
                web.route("GET", self.cam_prefix + "camaraysize", request_resolver(self.get_camaraysize, [("device_number", int)])),
                web.route("GET", self.cam_prefix + "startx", request_resolver(self.get_startx, [("device_number", int)])),
                web.route("GET", self.cam_prefix + "starty", request_resolver(self.get_starty, [("device_number", int)])),
                web.route("GET", self.cam_prefix + "pixelsizex", request_resolver(self.get_pixelsizex, [("device_number", int)])),
                web.route("GET", self.cam_prefix + "pixelsizey", request_resolver(self.get_pixelsizey, [("device_number", int)])),
                web.route("GET", self.cam_prefix + "ccdtemperature", request_resolver(self.get_ccdtemperature, [("device_number", int)])),
                web.route("GET", self.cam_prefix + "bitdepth", request_resolver(self.get_bitdepth, [("device_number", int)])),
                web.route("GET", self.cam_prefix + "imageready", request_resolver(self.image_ready, [("device_number", int)])),
                web.route("GET", self.cam_prefix + "imagearrayvariant", request_resolver(self.image_array_variant, [("device_number", int)])),
                web.route("PUT", self.cam_prefix + "startexposure", request_resolver(self.start_exposure, [("device_number", int)], [("Duration", float), ("Light", bool)])),
            ]
        )

        self.tel_prefix = '/api/v1/telescope/{device_number}/'
        self.server.router.add_routes(
            [
                web.route("GET", self.tel_prefix + "altitude", request_resolver(self.get_altitude, [("device_number", int)])),
                web.route("GET", self.tel_prefix + "azimuth", request_resolver(self.get_azimuth, [("device_number", int)])),
                web.route("GET", self.tel_prefix + "declination", request_resolver(self.get_declination, [("device_number", int)])),
                web.route("GET", self.tel_prefix + "rightascension", request_resolver(self.get_rightascension, [("device_number", int)])),
                web.route("GET", self.tel_prefix + "slewing", request_resolver(self.get_slewing, [("device_number", int)])),
                web.route("GET", self.tel_prefix + "tracking", request_resolver(self.get_tracking, [("device_number", int)])),
                web.route("PUT", self.tel_prefix + "slewtoaltaz", request_resolver(self.slew_altaz, [("device_number", int)], [("Azimuth", float), ("Altitude", float)])),
                web.route("PUT", self.tel_prefix + "slewtocoordinates", request_resolver(self.slew_radec, [("device_number", int)], [("RightAscension", float), ("Declination", float)])),

                web.route("GET", self.tel_prefix + "canpulseguide", request_resolver(self.canpulseguide, [("device_number", int)])),
                web.route("GET", self.tel_prefix + "cansetguiderates", request_resolver(self.cansetguiderates, [("device_number", int)])),
                web.route("GET", self.tel_prefix + "guideratedeclination", request_resolver(self.get_guideratedeclination, [("device_number", int)])),
                web.route("GET", self.tel_prefix + "guideraterightascension", request_resolver(self.get_guideraterightascension, [("device_number", int)])),
                web.route("PUT", self.tel_prefix + "guideratedeclination", request_resolver(self.guideratedeclination, [("device_number", int)], [("GuideRateDeclination", float)])),
                web.route("PUT", self.tel_prefix + "guideraterightascension", request_resolver(self.guideraterightascension, [("device_number", int)], [("GuideRateRightAscension", float)])),
                web.route("GET", self.tel_prefix + "ispulseguiding", request_resolver(self.ispulseguiding, [("device_number", int)])),
                web.route("PUT", self.tel_prefix + "pulseguide", request_resolver(self.pulseguiding, [("device_number", int)], [("Direction", int), ("Duration", float)])),
            ]
        )

    async def open(self) -> None:
        await Module.open(self)
        await self.server.run()

    async def close(self):
        await Module.close(self)

    async def ping(self, _):
        return web.Response(text="Pong!")

    async def action(self, _) -> web.Response:
        """
        Actions and SupportedActions are a standardised means for drivers to extend functionality beyond the built-in
        capabilities of the ASCOM device interfaces.
        (https://www.ascom-standards.org/api/#/ASCOM%20Methods%20Common%20To%20All%20Devices/put__device_type___device_number__action)
        """
        return web.HTTPBadRequest(reason="Action not found!")

    async def get_connected(self, device_type: str, device_number: int) -> web.StreamResponse:
        """
        Retrieves the connected state of the device.
        (https://www.ascom-standards.org/api/#/ASCOM%20Methods%20Common%20To%20All%20Devices/get__device_type___device_number__connected)
        """
        device = self._devices.get_device(device_type, device_number)

        if device.is_connected():
            return web.json_response((BoolResponse(value=True).toJson()))
        else:
            return web.json_response((BoolResponse(value=False).toJson()))

    async def set_connected(self, device_type: str, device_number: int, connected: bool) -> web.StreamResponse:
        """
        Sets the connected state of the device.
        (https://www.ascom-standards.org/api/#/ASCOM%20Methods%20Common%20To%20All%20Devices/put__device_type___device_number__connected)
        """
        device = self._devices.get_device(device_type, device_number)

        if connected and not device.is_connected():
            await device.connect()
        elif not connected and device.is_connected():
            device.disconnect()

        return web.json_response((BasicResponse().toJson()))

    # --- Camera Section ---

    async def get_binx(self, device_number: int) -> web.StreamResponse:
        """
        Returns the binning factor for the X axis.
        (https://www.ascom-standards.org/api/#/Camera%20Specific%20Methods/get_camera__device_number__binx)
        """
        camera: CameraState = self._devices.get_device("camera", device_number)

        binning = await camera.get_binning()
        return web.json_response((IntegerResponse(binning[0]).toJson()))

    async def get_biny(self, device_number: int) -> web.StreamResponse:
        """
        Returns the binning factor for the Y axis.
        (https://www.ascom-standards.org/api/#/Camera%20Specific%20Methods/get_camera__device_number__biny)
        """
        camera: CameraState = self._devices.get_device("camera", device_number)

        binning = await camera.get_binning()
        return web.json_response((IntegerResponse(binning[1]).toJson()))

    async def set_binx(self, device_number: int, binx: int) -> web.StreamResponse:
        """
        Sets the binning factor for the X axis.
        https://www.ascom-standards.org/api/#/Camera%20Specific%20Methods/put_camera__device_number__binx
        """
        camera: CameraState = self._devices.get_device("camera", device_number)

        await camera.set_binning(binx)
        return web.json_response((BasicResponse().toJson()))

    async def set_biny(self, device_number: int, biny: int) -> web.StreamResponse:
        """
        Sets the binning factor for the Y axis.
        https://www.ascom-standards.org/api/#/Camera%20Specific%20Methods/put_camera__device_number__biny
        """

        camera: CameraState = self._devices.get_device("camera", device_number)

        await camera.set_binning(biny)
        return web.json_response((BasicResponse().toJson()))

    async def get_camaraxsize(self, device_number: int) -> web.Response:
        """
        Returns the width of the CCD camera chip.
        (https://www.ascom-standards.org/api/#/Camera%20Specific%20Methods/get_camera__device_number__cameraxsize)
        """

        camera: CameraState = self._devices.get_device("camera", device_number)
        window = await camera.get_window()
        return web.json_response((IntegerResponse(window[2]).toJson()))

    async def get_camaraysize(self, device_number: int) -> web.Response:
        """
        Returns the height of the CCD camera chip.
        (https://www.ascom-standards.org/api/#/Camera%20Specific%20Methods/get_camera__device_number__cameraysize)
        """

        camera: CameraState = self._devices.get_device("camera", device_number)
        window = await camera.get_window()
        return web.json_response((IntegerResponse(window[3]).toJson()))

    async def get_startx(self, device_number: int) -> web.Response:
        """
        Return the current subframe X axis start position
        (https://www.ascom-standards.org/api/#/Camera%20Specific%20Methods/get_camera__device_number__startx)
        """

        camera: CameraState = self._devices.get_device("camera", device_number)
        window = await camera.get_window()
        return web.json_response((IntegerResponse(window[0]).toJson()))

    async def get_starty(self, device_number: int) -> web.Response:
        """
        Return the current subframe Y axis start position
        (https://www.ascom-standards.org/api/#/Camera%20Specific%20Methods/get_camera__device_number__starty)
        """

        camera: CameraState = self._devices.get_device("camera", device_number)
        window = await camera.get_window()
        return web.json_response((IntegerResponse(window[1]).toJson()))

    async def _get_pixsize(self, camera: CameraState) -> float:
        """
        Gets the detector pixel size from the header of an image
        Args:
            camera: camera state

        Returns:
            detector pixel size in microns
        """
        if camera._pix_size is not None:
            return camera._pix_size

        await camera.expose(0.00001, True)

        while not await camera.img_ready():
            pass

        filename = await camera.get_img()
        image: Image = await self.vfs.read_image(filename)

        camera._pix_size = 1000 * image.header['DET-PIXL']
        return camera._pix_size

    async def get_pixelsizex(self, device_number: int) -> web.Response:
        """
        Returns the width of the CCD chip pixels in microns.
        (https://www.ascom-standards.org/api/#/Camera%20Specific%20Methods/get_camera__device_number__pixelsizex)
        """

        camera: CameraState = self._devices.get_device("camera", device_number)
        pix_size = await self._get_pixsize(camera)
        return web.json_response((DoubleResponse(pix_size).toJson()))

    async def get_pixelsizey(self, device_number: int) -> web.Response:
        """
        Returns the height of the CCD chip pixels in microns.
        (https://www.ascom-standards.org/api/#/Camera%20Specific%20Methods/get_camera__device_number__pixelsizey)
        """

        camera: CameraState = self._devices.get_device("camera", device_number)
        pix_size = await self._get_pixsize(camera)
        return web.json_response((DoubleResponse(pix_size).toJson()))

    async def get_ccdtemperature(self, device_number: int) -> web.Response:
        """
        Returns the current CCD temperature in degrees Celsius.
        (https://www.ascom-standards.org/api/#/Camera%20Specific%20Methods/get_camera__device_number__ccdtemperature)
        """

        camera: CameraState = self._devices.get_device("camera", device_number)
        temp = await camera.get_temperature()
        return web.json_response((DoubleResponse(temp).toJson()))

    async def get_bitdepth(self, device_number: int) -> web.Response:
        """
        Returns the bitdepht of the image.
        Not supported by ascom alpaca!
        """

        camera: CameraState = self._devices.get_device("camera", device_number)
        depth = await camera.get_bit_depth()
        return web.json_response((IntegerResponse(depth).toJson()))

    async def image_ready(self, device_number: int) -> web.Response:
        """
        Returns a flag indicating whether the image is ready to be downloaded from the camera.
        https://www.ascom-standards.org/api/#/Camera%20Specific%20Methods/get_camera__device_number__imageready
        """

        camera: CameraState = self._devices.get_device("camera", device_number)
        state: bool = await camera.img_ready()

        return web.json_response((BoolResponse(value=state).toJson()))

    async def image_array_variant(self, device_number: int) -> web.Response:
        """
        Returns an array containing the pixel values from the last exposure.
        (https://www.ascom-standards.org/api/#/Camera%20Specific%20Methods/get_camera__device_number__imagearrayvariant)

        Always returns type 0. Bitdepth has to be aquiered by bitdetpth route.
        """


        camera: CameraState = self._devices.get_device("camera", device_number)

        filename: str = await camera.get_img()
        logging.info("Downloading..")
        image: Image = await self.vfs.read_image(filename)
        logging.info("Finished Download")
        return web.json_response((ImageArrayResponse(value=image.data, type=0).toJson()))

    async def start_exposure(self, device_number: int, duration: float, light: bool) -> web.Response:
        """
        Starts an exposure. Use ImageReady to check when the exposure is complete.
        (https://www.ascom-standards.org/api/#/Camera%20Specific%20Methods/put_camera__device_number__startexposure)
        """

        camera: CameraState = self._devices.get_device("camera", device_number)
        await camera.expose(float(duration), bool(light))

        return web.json_response((BasicResponse().toJson()))

    # --- Telescope Section ---

    async def get_declination(self, device_number: int):
        """
        The declination (degrees) of the mount's current equatorial coordinates, in the coordinate system given by
        the EquatorialSystem property.
        (https://www.ascom-standards.org/api/#/Telescope%20Specific%20Methods/get_telescope__device_number__declination)
        """
  
        telescope: TelescopeState = self._devices.get_device("telescope", device_number)
        _, dec = await telescope.get_telescope().get_radec()

        return web.json_response(DoubleResponse(dec).toJson())

    async def get_rightascension(self, device_number: int):
        """
        The right ascension (hours) of the mount's current equatorial coordinates, in the coordinate system given by
        the EquatorialSystem property.
        (https://www.ascom-standards.org/api/#/Telescope%20Specific%20Methods/get_telescope__device_number__rightascension)
        """

        telescope: TelescopeState = self._devices.get_device("telescope", device_number)
        ra, _ = await telescope.get_telescope().get_radec()
        ra = (ra % 360) / 360 * 24  # To deg to hour
        return web.json_response(DoubleResponse(ra).toJson())

    async def get_altitude(self, device_number: int):
        """
        The altitude above the local horizon of the mount's current position (degrees, positive up).
        (https://www.ascom-standards.org/api/#/Telescope%20Specific%20Methods/get_telescope__device_number__altitude)
        """

        telescope: TelescopeState = self._devices.get_device("telescope", device_number)
        alt, _ = await telescope.get_telescope().get_altaz()

        return web.json_response(DoubleResponse(alt).toJson())

    async def get_azimuth(self, device_number: int):
        """
        The azimuth at the local horizon of the mount's current position (degrees, North-referenced,
        positive East/clockwise).
        (https://www.ascom-standards.org/api/#/Telescope%20Specific%20Methods/get_telescope__device_number__azimuth)
        """

        telescope: TelescopeState = self._devices.get_device("telescope", device_number)
        _, az = await telescope.get_telescope().get_altaz()

        return web.json_response(DoubleResponse(az).toJson())

    async def get_slewing(self, device_number: int):
        
        telescope: TelescopeState = self._devices.get_device("telescope", device_number)
        status = await telescope.get_telescope().get_motion_status()

        return web.json_response(BoolResponse(status == MotionStatus.SLEWING).toJson())

    async def get_tracking(self, device_number: int):

        telescope: TelescopeState = self._devices.get_device("telescope", device_number)
        status = await telescope.get_telescope().get_motion_status()

        return web.json_response(BoolResponse(status == MotionStatus.TRACKING).toJson())

    async def slew_radec(self, device_number: int, rightascension: float, declination: float):
        """
        Matches the scope's equatorial coordinates to the given equatorial coordinates.
        (https://www.ascom-standards.org/api/#/Telescope%20Specific%20Methods/put_telescope__device_number__synctocoordinates)
        """

        telescope: TelescopeState = self._devices.get_device("telescope", device_number)

        ra: float = float(rightascension) / 24 * 360
        dec: float = float(declination)

        asyncio.create_task(telescope.get_telescope().move_radec(ra, dec))

        return web.json_response((BasicResponse().toJson()))

    async def slew_altaz(self, device_number: int, azimuth: float, altitude:float):
        """
        Move the telescope to the given local horizontal coordinates, return when slew is complete.
        (https://www.ascom-standards.org/api/#/Telescope%20Specific%20Methods/put_telescope__device_number__slewtoaltaz)
        """

        telescope: TelescopeState = self._devices.get_device("telescope", device_number)
        
        alt: float = float(altitude)
        az: float = float(azimuth)

        await telescope.get_telescope().move_altaz(alt, az)
        return web.json_response((BasicResponse().toJson()))

    async def canpulseguide(self, device_number: int):
        """
        True if this telescope is capable of software-pulsed guiding (via the PulseGuide(GuideDirections, Int32) method).
        https://www.ascom-standards.org/api/?urls.primaryName=ASCOM%20Alpaca%20Device%20API#/Telescope%20Specific%20Methods/get_telescope__device_number__canpulseguide
        """

        telescope: TelescopeState = self._devices.get_device("telescope", device_number)

        return web.json_response((BoolResponse(True).toJson()))

    async def cansetguiderates(self, device_number: int):
        """
        True if the guide rate properties used for PulseGuide(GuideDirections, Int32) can ba adjusted.
        https://www.ascom-standards.org/api/?urls.primaryName=ASCOM%20Alpaca%20Device%20API#/Telescope%20Specific%20Methods/get_telescope__device_number__cansetguiderates
        """

        _: TelescopeState = self._devices.get_device("telescope", device_number)

        return web.json_response((BoolResponse(True).toJson()))

    async def get_guideratedeclination(self, device_number: int):
        """
        The current Declination movement rate offset for telescope guiding (degrees/sec)
        https://www.ascom-standards.org/api/?urls.primaryName=ASCOM%20Alpaca%20Device%20API#/Telescope%20Specific%20Methods/get_telescope__device_number__guideratedeclination
        """
        telescope: TelescopeState = self._devices.get_device("telescope", device_number)

        dec = telescope.get_guide_rates()[1]
        return web.json_response((DoubleResponse(dec).toJson()))

    async def guideratedeclination(self, device_number: int, guideratedeclination: float):
        """
        Sets the current Declination movement rate offset for telescope guiding (degrees/sec).
        https://www.ascom-standards.org/api/?urls.primaryName=ASCOM%20Alpaca%20Device%20API#/Telescope%20Specific%20Methods/get_telescope__device_number__guideratedeclination
        """

        telescope: TelescopeState = self._devices.get_device("telescope", device_number)
        telescope.set_guide_rate_dec(float(guideratedeclination))
        return web.json_response((BasicResponse().toJson()))

    async def get_guideraterightascension(self, device_number: int):
        """
        TThe current RightAscension movement rate offset for telescope guiding (degrees/sec).
        https://www.ascom-standards.org/api/?urls.primaryName=ASCOM%20Alpaca%20Device%20API#/Telescope%20Specific%20Methods/get_telescope__device_number__guideraterightascension
        """

        telescope: TelescopeState = self._devices.get_device("telescope", device_number)

        ra = telescope.get_guide_rates()[0]
        return web.json_response((DoubleResponse(ra).toJson()))

    @request_resolver(["device_type", "device_number"], ["GuideRateRightAscension"])
    async def guideraterightascension(self, device_number: int, guideraterightascension: float):
        """
        Sets the current RightAscension movement rate offset for telescope guiding (degrees/sec).
        https://www.ascom-standards.org/api/?urls.primaryName=ASCOM%20Alpaca%20Device%20API#/Telescope%20Specific%20Methods/put_telescope__device_number__guideraterightascension
        """

        telescope: TelescopeState = self._devices.get_device("telescope", device_number)

        telescope.set_guide_rate_dec(float(guideraterightascension))
        return web.json_response((BasicResponse().toJson()))

    async def ispulseguiding(self, device_number: int):
        """
        True if a PulseGuide(GuideDirections, Int32) command is in progress, False otherwise.
        https://www.ascom-standards.org/api/?urls.primaryName=ASCOM%20Alpaca%20Device%20API#/Telescope%20Specific%20Methods/get_telescope__device_number__ispulseguiding
        """

        telescope: TelescopeState = self._devices.get_device("telescope", device_number)
        result = telescope.is_pulse_guiding()

        return web.json_response((BoolResponse(result).toJson()))

    async def pulseguiding(self, device_number: int, direction: int, duration: float):
        """
        Moves the scope in the given direction for the given interval or time at the rate given by the corresponding guide rate property.
        https://www.ascom-standards.org/api/?urls.primaryName=ASCOM%20Alpaca%20Device%20API#/Telescope%20Specific%20Methods/put_telescope__device_number__pulseguide
        """

        telescope: TelescopeState = self._devices.get_device("telescope", device_number)
        await telescope.pulse_guide(direction, duration) #asyncio.create_task

        return web.json_response((BasicResponse().toJson()))

from pyobs.comm import Comm

from pyobs_alpaca_server.devices.camera import CameraState
from pyobs_alpaca_server.devices.telescope import TelescopeState


class BadDeviceType(Exception):
    pass


class BadDeviceNumber(Exception):
    pass


class DeviceStorage:
    def __init__(self, devices, comm: Comm):
        self._devices = {
            'telescope': [TelescopeState(name, comm) for name in devices['telescope']],
            'camera': [CameraState(name, comm) for name in devices['camera']]
        }

    def get_device(self, device_type: str, device_number: int):
        """
        Get a connected device interface for a device type and device number specified in the request
        Args:
            device_type: Type (telescope, camera) of the device
            device_number: (API) number of the device
        Returns:
            device state

        Raises:
            BadDeviceType
            BadDeviceNumber
        """

        if device_type not in self._devices:
            raise BadDeviceType()

        if int(device_number) >= len(self._devices[device_type]):
            raise BadDeviceNumber()

        return self._devices[device_type][int(device_number)]

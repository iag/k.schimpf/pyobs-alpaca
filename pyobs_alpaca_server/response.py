from typing import Any

from numpy.typing import NDArray


class BasicResponse:
    def __init__(self, client_transaction_id: int = 0, server_transaction_id: int = 0, error_number: int = 0,
                 error_msg: str = ""):
        self.client_transaction_id = client_transaction_id
        self.server_transaction_id = server_transaction_id

        self.error_number = error_number
        self.error_msg = error_msg

    def toJson(self):
        return {
            'ClientTransactionID': self.client_transaction_id,
            'ServerTransactionID': self.server_transaction_id,
            'ErrorNumber': self.error_number,
            'ErrorMessage': self.error_msg
        }


class BoolResponse(BasicResponse):
    def __init__(self, value: bool, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.value = value

    def toJson(self):
        response = BasicResponse.toJson(self)
        response['Value'] = self.value

        return response


class DoubleResponse(BasicResponse):
    def __init__(self, value: float, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.value = value

    def toJson(self):
        response = BasicResponse.toJson(self)
        response['Value'] = self.value

        return response


class IntegerResponse(BasicResponse):
    def __init__(self, value: float, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.value = value

    def toJson(self):
        response = BasicResponse.toJson(self)
        response['Value'] = int(self.value)

        return response


class ImageArrayResponse(BasicResponse):
    def __init__(self, value: NDArray[Any], type: int = 2, rank: int = 2, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.value = value.tolist()
        self.type = type
        self.rank = rank

    def toJson(self):
        response = BasicResponse.toJson(self)
        response['Value'] = self.value
        response['Type'] = self.type
        response['Rank'] = self.rank
        return response

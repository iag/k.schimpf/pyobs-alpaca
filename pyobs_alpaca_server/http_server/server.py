import logging

from aiohttp import web


class Server:
    """
    Improvised webserver
    """
    def __init__(self, host: str = 'localhost', port: int = 8080):
        self.host = host
        self.port = port

        self.router = web.UrlDispatcher()

        self.server: web.Server = None
        self.runner: web.ServerRunner = None
        self.site: web.TCPSite = None

    async def run(self):
        self.server = web.Server(self._handler, access_log=logging.Logger(logging.ERROR)) #
        self.runner = web.ServerRunner(self.server)
        await self.runner.setup()
        self.site = web.TCPSite(self.runner, self.host, self.port)
        await self.site.start()

        print(f"======= Serving on http://{self.host}:{self.port}/ ======")

    async def _handler(self, request: web.BaseRequest):
        match = await self.router.resolve(request)
        if 'pattern' in match.get_info():
            request['info'] = match.get_info()['pattern'].match(request.path)

        return await match.handler(request)

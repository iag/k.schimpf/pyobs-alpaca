import functools

from aiohttp import web


def to_type(data: str, cast_type: type):
    if cast_type == bool:
        return data.lower() == "true"

    return cast_type(data)


def compile_route_parameters(request: web.Request, parameters) -> dict:
    output = {
        parameter_name: to_type(request["info"][parameter_name], parameter_type) for parameter_name, parameter_type in parameters
    }

    return output


async def compile_body_parameters(request: web.Request, parameters) -> dict:
    data = await request.post()

    output = {
        parameter_name.lower(): to_type(data[parameter_name], parameter_type) for parameter_name, parameter_type in parameters
    }

    return output


def request_resolver(handler, route_parameters=None, body_parameters=None):
    if route_parameters is None:
        route_parameters = []

    if body_parameters is None:
        body_parameters = []

    @functools.wraps(handler)
    async def wrapped(request):
        kwargs = {}
        try:
            kwargs.update(compile_route_parameters(request, route_parameters))
        except KeyError:
            return web.HTTPBadRequest()

        try:
            kwargs.update(await compile_body_parameters(request, body_parameters))
        except KeyError:
            return web.HTTPBadRequest()

        return await handler(**kwargs)
    return wrapped

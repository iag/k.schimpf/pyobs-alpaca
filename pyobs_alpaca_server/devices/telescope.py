from pyobs.comm import Comm
from pyobs.interfaces import ITelescope, IOffsetsRaDec


class TelescopeState:
    """
    Class to manage the telescope state.
    """
    def __init__(self, name: str, comm: Comm):
        self._name = name
        self._telescope = None
        self._comm: Comm = comm

        self._is_pulse_guiding: bool = False
        self._guide_rate_dec: float = 0.01 # Deg per sec
        self._guide_rate_ra: float = 0.01 # Deg per sec

    def get_name(self) -> str:
        return self._name

    async def connect(self):
        self._telescope = await self._comm.proxy(self._name, ITelescope)

    def disconnect(self):
        self._telescope = None

    def is_connected(self):
        return self._telescope is not None
    
    def get_guide_rates(self) -> (float, float):
        return self._guide_rate_ra, self._guide_rate_dec

    def set_guide_rate_dec(self, rate: float):
        self._guide_rate_dec = rate

    def set_guide_rate_ra(self, rate: float):
        self._guide_rate_ra = rate

    def is_pulse_guiding(self) -> bool:
        return self._is_pulse_guiding

    def _calc_pulse(self, direction: int, time: float) -> (float, float):
        if direction == 0:
            return 0.0, time / 1000 * self._guide_rate_dec
        elif direction == 1:
            return 0.0, -time / 1000 * self._guide_rate_dec
        elif direction == 2:
            return time / 1000 * self._guide_rate_ra, 0.0
        elif direction == 3:
            return -time / 1000 * self._guide_rate_ra, 0.0
        else:
            raise ValueError("Direction must be between 0 and 3!")

    async def pulse_guide(self, direction: int, time: float):
        if self.is_pulse_guiding():
            raise PermissionError("The telescope is already pulse guiding!")

        if isinstance(self._telescope, IOffsetsRaDec):
            ra, dec = await self._telescope.get_offsets_radec() # Current Offset
            dra, ddec = self._calc_pulse(direction, time)

            self._is_pulse_guiding = True
            print(f"Setting Offset {ra + dra} - {dec + ddec}")
            await self._telescope.set_offsets_radec(ra + dra, dec + ddec)
            print("Finished Offset")
            self._is_pulse_guiding = False

        else:
            raise ConnectionError("Offset Interface not available!")

    def get_telescope(self):
        return self._telescope

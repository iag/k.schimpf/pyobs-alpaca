import asyncio
from typing import Tuple

from pyobs.comm import Comm
from pyobs.interfaces import ICamera, IImageType, IExposureTime, IWindow, ITemperatures, IBinning, IImageFormat
from pyobs.utils.enums import ImageType, ExposureStatus, ImageFormat


class CameraState:
    """
    Class to manage the camera state.
    """
    def __init__(self, name: str, comm: Comm):
        self._name = name
        self._camera: ICamera = None
        self._comm: Comm = comm
        self._last_exposure = None
        self._pix_size = None

    def get_name(self) -> str:
        return self._name

    async def connect(self):
        self._camera = await self._comm.proxy(self._name, ICamera)

    def disconnect(self):
        self._camera = None

    def is_connected(self):
        return self._camera is not None

    async def expose(self, duration: float, light: bool) -> None:
        """
        Exposes an image
        Args:
            duration: Exposure duration
            light: If light oder dark frame
        """
        if isinstance(self._camera, IExposureTime):
            await self._camera.set_exposure_time(duration)

        if isinstance(self._camera, IImageType):
            if light:
                await self._camera.set_image_type(ImageType.OBJECT)
            else:
                await self._camera.set_image_type(ImageType.DARK)

        self._last_exposure = asyncio.create_task(self._camera.grab_data(broadcast=True))

    async def img_ready(self) -> bool:
        """
        Checks if an image is ready
        Returns:
            if an image is ready

        """
        return self._last_exposure is not None and (await self._camera.get_exposure_status() == ExposureStatus.IDLE)

    async def get_img(self) -> str:
        """
        Gets the last exposed image name
        Returns:
            last exposed image name

        """
        return await self._last_exposure

    async def get_window(self) -> [int, int, int, int]:
        if isinstance(self._camera, IWindow):
            return await self._camera.get_window()
        else:
            return [0, 0, 0, 0]

    async def get_temperature(self) -> float:
        if isinstance(self._camera, ITemperatures):
            temps = await self._camera.get_temperatures()
            return temps['CCD']
        else:
            return 0

    async def get_binning(self) -> Tuple[int, int]:
        if isinstance(self._camera, IBinning):
            return await self._camera.get_binning()
        else:
            return 0, 0

    async def set_binning(self, binning: int) -> None:
        if isinstance(self._camera, IBinning):
            available = await self._camera.list_binnings()
            current = [binning, binning]

            if current not in available:
                raise ValueError()

            await self._camera.set_binning(*current)

    async def set_format(self, image_format: ImageFormat):
        if isinstance(self._camera, IImageFormat):
            await self._camera.set_image_format(image_format)

    async def get_format(self) -> ImageFormat:
        if isinstance(self._camera, IImageFormat):
            return await self._camera.get_image_format()
        else:
            return ImageFormat.INT8

    async def get_bit_depth(self) -> int:
        fmt = await self.get_format()

        if fmt == ImageFormat.INT8:
            return 8
        elif fmt == ImageFormat.INT16:
            return 16
        elif fmt == ImageFormat.FLOAT32:
            return 32
        elif fmt == ImageFormat.FLOAT64:
            return 64
        elif fmt == ImageFormat.RGB24:
            return 24

        raise ValueError()
